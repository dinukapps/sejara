package app.fara.com.sejara.Entity;

/**
 * Created by dinuka on 15/03/2017.
 */

public class Chapter {
    private String ID = "";
    private String Name = "";

    public Chapter() {

    }

    public Chapter(String ID, String name) {
        this.setID(ID);
        setName(name);
    }


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
