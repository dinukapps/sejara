package app.fara.com.sejara.Entity;

/**
 * Created by dinuka on 16/03/2017.
 */

public class Note {
    private String ID = "";
    private String Title = "";
    private String Civillization = "";
    private String Body = "";

    public Note() {

    }

    public Note(String ID, String title, String civillization, String body) {
        this.ID = ID;
        this.Title = title;
        this.Civillization = civillization;
        this.Body = body;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getCivillization() {
        return Civillization;
    }

    public void setCivillization(String civillization) {
        Civillization = civillization;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }
}
