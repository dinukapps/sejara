package app.fara.com.sejara.Views;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.fara.com.sejara.R;

public class HomeView extends AppCompatActivity {

    TextView title;
    RelativeLayout row_mynotes, row_quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_view);

        HomeView.this.getSupportActionBar().hide();

        title = (TextView) findViewById(R.id.title);
        row_mynotes = (RelativeLayout) findViewById(R.id.row_mynotes);
        row_quiz = (RelativeLayout) findViewById(R.id.row_quiz);

        Typeface billabong_typeFace = Typeface.createFromAsset(getAssets(), "fonts/Billabong.ttf");
        title.setTypeface(billabong_typeFace);

        row_mynotes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        row_mynotes.setBackgroundResource(R.drawable.flat_button_white_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        row_mynotes.setBackgroundResource(R.drawable.flat_button_white);
                        break;
                    case MotionEvent.ACTION_UP:
                        row_mynotes.setBackgroundResource(R.drawable.flat_button_white);

                        Intent intent = new Intent(HomeView.this, MyNotesView.class);
                        startActivity(intent);

                        break;
                }
                return true;
            }
        });

        row_quiz.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        row_quiz.setBackgroundResource(R.drawable.flat_button_white_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        row_quiz.setBackgroundResource(R.drawable.flat_button_white);
                        break;
                    case MotionEvent.ACTION_UP:
                        row_quiz.setBackgroundResource(R.drawable.flat_button_white);

                        Intent intent = new Intent(HomeView.this, QuizView.class);
                        startActivity(intent);

                        break;
                }
                return true;
            }
        });

    }
}
