package app.fara.com.sejara.Views.CardViewer;


public class CardItem {

    private int mTextResource;
    private int mTitleResource;
    private int mImageResource;
    private String mTitleString;
    private String mID;
    private String mPicture;

    /*
    public CardItem(int title, int text) {
        mTitleResource = title;
        mTextResource = text;
    }
    */

    public CardItem(String title, int description) {
        mTitleString = title;
        mTextResource = description;
    }

    public CardItem(String id, String title, int description) {
        mID = id;
        mTitleString = title;
        mTextResource = description;
    }

    public CardItem(String id, String title, String picture, int description) {
        mID = id;
        mTitleString = title;
        mTextResource = description;
        mPicture = picture;
    }

    public CardItem(int title, int image, String extra) {
        mTitleResource = title;
        mImageResource = image;
    }

    public int getText() {
        return mTextResource;
    }

    public String getTitle() {
        return mTitleString;
    }

    public int getImage() {
        return mImageResource;
    }

    public String getID() {
        return mID;
    }

    public String getPicture() {
        return mPicture;
    }
}
