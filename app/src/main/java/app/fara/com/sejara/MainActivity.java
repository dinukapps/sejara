package app.fara.com.sejara;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.drawee.backends.pipeline.Fresco;

import app.fara.com.sejara.Views.HomeView;
import app.fara.com.sejara.Views.WelcomeActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(MainActivity.this);
        setContentView(R.layout.activity_main);

        startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
        finish();

    }
}
