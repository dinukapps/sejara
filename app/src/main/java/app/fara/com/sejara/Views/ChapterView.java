package app.fara.com.sejara.Views;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.fara.com.sejara.Entity.Chapter;
import app.fara.com.sejara.Entity.Civillization;
import app.fara.com.sejara.R;
import app.fara.com.sejara.Views.CardViewer.CardFragmentPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.CardItem;
import app.fara.com.sejara.Views.CardViewer.CardPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.ImageCardPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.ShadowTransformer;

public class ChapterView extends AppCompatActivity {

    private ViewPager mViewPager;
    private RelativeLayout backBtn;
    private ImageCardPagerAdapter mCardAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private CardFragmentPagerAdapter mFragmentCardAdapter;
    private RelativeLayout row_1;
    private String chapter_id, chapter_title;
    private RelativeLayout rlProgress;
    private DatabaseReference mDatabase;
    private int civilCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_view);

        chapter_id = getIntent().getStringExtra("chapter_id");
        chapter_title = getIntent().getStringExtra("chapter_title");

        ChapterView.this.getSupportActionBar().hide();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        backBtn = (RelativeLayout) findViewById(R.id.backBtn);

        rlProgress.setVisibility(View.VISIBLE);

        new LoadCivillizations().execute();

        mCardAdapter = new ImageCardPagerAdapter(ChapterView.this);

        backBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        backBtn.setBackgroundColor(Color.parseColor("#009AFC"));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        backBtn.setBackgroundColor(Color.parseColor("#009cff"));
                        break;
                    case MotionEvent.ACTION_UP:
                        backBtn.setBackgroundColor(Color.parseColor("#009cff"));

                        finish();

                        break;
                }
                return true;
            }
        });

        /*
        row_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        row_1.setBackgroundResource(R.drawable.flat_button_white_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        row_1.setBackgroundResource(R.drawable.flat_button_white_box);
                        break;
                    case MotionEvent.ACTION_UP:
                        row_1.setBackgroundResource(R.drawable.flat_button_white_box);

                        Intent intent = new Intent(ChapterView.this, DetailsView.class);
                        intent.putExtra("category", "Sistem Pemerintahan");
                        startActivity(intent);

                        break;
                }
                return true;
            }
        });
        */

    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    class LoadCivillizations extends AsyncTask<Void, Void, Void> {

        LoadCivillizations() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            mDatabase.child("civillizations").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                        //mCardAdapter.addCardItem(new CardItem(R.string.topic_1, R.drawable.image_yunani, ""));

                        Civillization civil = postSnapshot.getValue(Civillization.class);
                        mCardAdapter.addCardItem(new CardItem(civil.getID(), civil.getName(), civil.getPicture(), R.string.text_1));
                        civilCount++;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mFragmentCardAdapter = new CardFragmentPagerAdapter(civilCount,  getSupportFragmentManager(), dpToPixels(2, ChapterView.this));

                            mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);

                            mViewPager.setAdapter(mCardAdapter);
                            mViewPager.setPageTransformer(false, mCardShadowTransformer);
                            mViewPager.setOffscreenPageLimit(3);
                            mCardShadowTransformer.enableScaling(true);

                            rlProgress.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }

}
