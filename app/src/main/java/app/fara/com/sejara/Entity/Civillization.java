package app.fara.com.sejara.Entity;

/**
 * Created by dinuka on 16/03/2017.
 */

public class Civillization {
    private String ID = "";
    private String Name = "";
    private String Picture = "";

    public Civillization() {

    }

    public Civillization(String ID, String name, String picture) {
        this.ID = ID;
        Name = name;
        Picture = picture;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }
}
