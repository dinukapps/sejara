package app.fara.com.sejara.Views;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.enums.SnackbarType;

import java.util.ArrayList;
import java.util.Arrays;

import app.fara.com.sejara.Entity.Question;
import app.fara.com.sejara.R;

public class QuizView extends AppCompatActivity {

    TextView label_question_value;
    RelativeLayout answer_1, answer_2, answer_3, answer_4;
    TextView answer_1_value, answer_2_value, answer_3_value, answer_4_value;
    RelativeLayout panel_correct_answer, panel_wrong_answer, panel_endquiz, verdict_correct_button, verdict_wrong_button, verdict_end_button;
    ArrayList<Question> questions = new ArrayList<>();
    int currentQuestion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_view);

        QuizView.this.getSupportActionBar().hide();

        label_question_value = (TextView) findViewById(R.id.label_question_value);
        answer_1 = (RelativeLayout) findViewById(R.id.answer_1);
        answer_2 = (RelativeLayout) findViewById(R.id.answer_2);
        answer_3 = (RelativeLayout) findViewById(R.id.answer_3);
        answer_4 = (RelativeLayout) findViewById(R.id.answer_4);
        answer_1_value = (TextView) findViewById(R.id.answer_1_value);
        answer_2_value = (TextView) findViewById(R.id.answer_2_value);
        answer_3_value = (TextView) findViewById(R.id.answer_3_value);
        answer_4_value = (TextView) findViewById(R.id.answer_4_value);
        panel_correct_answer = (RelativeLayout) findViewById(R.id.panel_correct_answer);
        panel_wrong_answer = (RelativeLayout) findViewById(R.id.panel_wrong_answer);
        panel_endquiz = (RelativeLayout) findViewById(R.id.panel_endquiz);
        verdict_correct_button = (RelativeLayout) findViewById(R.id.verdict_correct_button);
        verdict_wrong_button = (RelativeLayout) findViewById(R.id.verdict_wrong_button);
        verdict_end_button = (RelativeLayout) findViewById(R.id.verdict_end_button);

        GenerateQuestions();

        StartQuiz();

        //region answer clicks
        answer_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        answer_1.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        answer_1.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        answer_1.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        if(questions.get(currentQuestion).getCorrectAnswer() == 1){
                            ProcessCorrectAnswer();
                        }
                        else{
                            ProcessWrongAnswer();
                        }

                        break;
                }
                return true;
            }
        });
        answer_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        answer_2.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        answer_2.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        answer_2.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        if(questions.get(currentQuestion).getCorrectAnswer() == 2){
                            ProcessCorrectAnswer();
                        }
                        else{
                            ProcessWrongAnswer();
                        }

                        break;
                }
                return true;
            }
        });
        answer_3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        answer_3.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        answer_3.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        answer_3.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        if(questions.get(currentQuestion).getCorrectAnswer() == 3){
                            ProcessCorrectAnswer();
                        }
                        else{
                            ProcessWrongAnswer();
                        }

                        break;
                }
                return true;
            }
        });
        answer_4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        answer_4.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        answer_4.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        answer_4.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        if(questions.get(currentQuestion).getCorrectAnswer() == 4){
                            ProcessCorrectAnswer();
                        }
                        else{
                            ProcessWrongAnswer();
                        }

                        break;
                }
                return true;
            }
        });
        verdict_end_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        verdict_end_button.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        verdict_end_button.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        verdict_end_button.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        finish();

                        break;
                }
                return true;
            }
        });
        //endregion

        verdict_correct_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
                panel_correct_answer.setAnimation(anim);
                panel_correct_answer.setVisibility(View.GONE);
                currentQuestion++;
                StartQuiz();
            }
        });

        verdict_wrong_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
                panel_wrong_answer.setAnimation(anim);
                panel_wrong_answer.setVisibility(View.GONE);
            }
        });

    }

    private void StartQuiz() {
        switch(currentQuestion){
            case 0:
                label_question_value.setText(questions.get(0).getQuestion());
                answer_1_value.setText(questions.get(0).getAnswers().get(0));
                answer_2_value.setText(questions.get(0).getAnswers().get(1));
                answer_3_value.setText(questions.get(0).getAnswers().get(2));
                answer_4_value.setText(questions.get(0).getAnswers().get(3));
                break;
            case 1:
                label_question_value.setText(questions.get(1).getQuestion());
                answer_1_value.setText(questions.get(1).getAnswers().get(0));
                answer_2_value.setText(questions.get(1).getAnswers().get(1));
                answer_3_value.setText(questions.get(1).getAnswers().get(2));
                answer_4_value.setText(questions.get(1).getAnswers().get(3));
                break;
            case 2:
                label_question_value.setText(questions.get(2).getQuestion());
                answer_1_value.setText(questions.get(2).getAnswers().get(0));
                answer_2_value.setText(questions.get(2).getAnswers().get(1));
                answer_3_value.setText(questions.get(2).getAnswers().get(2));
                answer_4_value.setText(questions.get(2).getAnswers().get(3));
                break;
            case 3:
                label_question_value.setText(questions.get(3).getQuestion());
                answer_1_value.setText(questions.get(3).getAnswers().get(0));
                answer_2_value.setText(questions.get(3).getAnswers().get(1));
                answer_3_value.setText(questions.get(3).getAnswers().get(2));
                answer_4_value.setText(questions.get(3).getAnswers().get(3));
                break;
            case 4:
                ShowEnd();
                break;
        }
    }

    private void ShowEnd() {
        MediaPlayer mediaPlayer= MediaPlayer.create(QuizView.this, R.raw.success);
        mediaPlayer.start();
        panel_endquiz.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        panel_endquiz.setAnimation(anim);
    }

    private void ProcessWrongAnswer() {
        MediaPlayer mediaPlayer= MediaPlayer.create(QuizView.this, R.raw.wrong);
        mediaPlayer.start();
        panel_wrong_answer.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        panel_wrong_answer.setAnimation(anim);
    }

    private void ProcessCorrectAnswer() {
        MediaPlayer mediaPlayer= MediaPlayer.create(QuizView.this, R.raw.success);
        mediaPlayer.start();
        panel_correct_answer.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        panel_correct_answer.setAnimation(anim);
    }

    private void GenerateQuestions() {
        Question question = new Question();
        question.setQuestion("What is the capital of Malaysia?");
        question.setCorrectAnswer(2);
        question.setAnswers(new ArrayList<>(Arrays.asList("Petaling Jaya", "Kuala Lumpur", "North America", "Argentina")));
        questions.add(question);

        question = new Question();
        question.setQuestion("Who founded Apple?");
        question.setCorrectAnswer(3);
        question.setAnswers(new ArrayList<>(Arrays.asList("Steve Balmer", "Harry Potter", "Steve Jobs", "Donald Trump")));
        questions.add(question);

        question = new Question();
        question.setQuestion("Which came first? Egg or the Chicken?");
        question.setCorrectAnswer(1);
        question.setAnswers(new ArrayList<>(Arrays.asList("Egg", "Chicken", "Rooster", "Monkey")));
        questions.add(question);

        question = new Question();
        question.setQuestion("How many Knowledge areas mentioned in PMBOK?");
        question.setCorrectAnswer(4);
        question.setAnswers(new ArrayList<>(Arrays.asList("20", "4", "2", "10")));
        questions.add(question);
    }
}
