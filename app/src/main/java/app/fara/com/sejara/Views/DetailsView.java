package app.fara.com.sejara.Views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.text.Html;
import android.text.Spanned;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.enums.SnackbarType;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import app.fara.com.sejara.Entity.Civillization;
import app.fara.com.sejara.Entity.Note;
import app.fara.com.sejara.R;
import app.fara.com.sejara.Views.CardViewer.CardFragmentPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.CardItem;
import app.fara.com.sejara.Views.CardViewer.ShadowTransformer;

public class DetailsView extends AppCompatActivity implements Html.ImageGetter {

    private String note_id, note_title;
    private TextView viewTitle, body;
    private RelativeLayout contextBtn, menu, closeMenu;
    private RelativeLayout row_addbookmark, row_back;
    private RelativeLayout rlProgress;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_view);

        note_id = getIntent().getStringExtra("note_id");
        note_title = getIntent().getStringExtra("note_title");

        DetailsView.this.getSupportActionBar().hide();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        body = (TextView) findViewById(R.id.body);
        viewTitle = (TextView) findViewById(R.id.viewTitle);
        contextBtn = (RelativeLayout) findViewById(R.id.contextBtn);
        menu = (RelativeLayout) findViewById(R.id.menu);
        closeMenu = (RelativeLayout) findViewById(R.id.closeMenu);
        row_addbookmark = (RelativeLayout) findViewById(R.id.row_addbookmark);
        row_back = (RelativeLayout) findViewById(R.id.row_back);

        rlProgress.setVisibility(View.VISIBLE);
        menu.setVisibility(View.GONE);

        viewTitle.setText(note_title);

        new LoadNote().execute();

        contextBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        contextBtn.setBackgroundColor(Color.parseColor("#009AFC"));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        contextBtn.setBackgroundColor(Color.parseColor("#009cff"));
                        break;
                    case MotionEvent.ACTION_UP:
                        contextBtn.setBackgroundColor(Color.parseColor("#009cff"));

                        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                        menu.setAnimation(anim);
                        menu.setVisibility(View.VISIBLE);

                        break;
                }
                return true;
            }
        });

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CloseMenu();
            }
        });

        row_addbookmark.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        row_addbookmark.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        row_addbookmark.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        row_addbookmark.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        CloseMenu();

                        Snackbar.with(DetailsView.this)
                                .type(SnackbarType.MULTI_LINE)
                                .text("Added Bookmark! You can access it from the Bookmarks view")
                                .textColor(Color.parseColor("#ffffff"))
                                .color(Color.parseColor("#009cff"))
                                .show(DetailsView.this);

                        break;
                }
                return true;
            }
        });

        row_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        row_back.setBackgroundResource(R.drawable.flat_button_white_box_lowpad_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        row_back.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);
                        break;
                    case MotionEvent.ACTION_UP:
                        row_back.setBackgroundResource(R.drawable.flat_button_white_box_lowpad);

                        CloseMenu();

                        finish();

                        break;
                }
                return true;
            }
        });

    }

    private void CloseMenu(){
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        menu.setAnimation(anim);
        menu.setVisibility(View.GONE);
    }

    @Override
    public Drawable getDrawable(String source) {
        LevelListDrawable d = new LevelListDrawable();
        Drawable empty = getResources().getDrawable(R.mipmap.ic_launcher);
        d.addLevel(0, 0, empty);
        d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());

        new LoadImage().execute(source, d);

        return d;
    }

    class LoadNote extends AsyncTask<Void, Void, Void> {

        LoadNote() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            mDatabase.child("notes/" + note_id).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    final Note note = dataSnapshot.getValue(Note.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Spanned spanned = Html.fromHtml(note.getBody(), DetailsView.this, null);
                            body.setText(spanned);

                            rlProgress.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }

    class LoadImage extends AsyncTask<Object, Void, Bitmap> {

        private LevelListDrawable mDrawable;

        @Override
        protected Bitmap doInBackground(Object... params) {
            String source = (String) params[0];
            mDrawable = (LevelListDrawable) params[1];
            try {
                InputStream is = new URL(source).openStream();
                return BitmapFactory.decodeStream(is);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                BitmapDrawable d = new BitmapDrawable(bitmap);
                mDrawable.addLevel(1, 1, d);
                mDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
                mDrawable.setLevel(1);
                // i don't know yet a better way to refresh TextView
                // mTv.invalidate() doesn't work as expected
                CharSequence t = body.getText();
                body.setText(t);
            }
        }
    }

}
