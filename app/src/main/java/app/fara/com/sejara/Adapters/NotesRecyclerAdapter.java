package app.fara.com.sejara.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

import app.fara.com.sejara.Entity.Note;
import app.fara.com.sejara.R;
import app.fara.com.sejara.Views.DetailsView;
import app.fara.com.sejara.Views.NotesSelectionActivity;

/**
 * Created by dinuka on 16/03/2017.
 */

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.ViewHolder> {

    private List<Note> items;
    private Context context;

    public NotesRecyclerAdapter(List<Note> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_note_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        final Note item = items.get(position);
        holder.note_title.setText(item.getTitle());

        final ViewHolder hl = holder;
        hl.parent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        hl.parent.setBackgroundResource(R.drawable.flat_button_white_box_touched);
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        hl.parent.setBackgroundResource(R.drawable.flat_button_white_box);
                        break;
                    case MotionEvent.ACTION_UP:
                        hl.parent.setBackgroundResource(R.drawable.flat_button_white_box);

                        Intent intent = new Intent(context, DetailsView.class);
                        intent.putExtra("note_id", item.getID());
                        intent.putExtra("note_title", item.getTitle());
                        context.startActivity(intent);

                        break;
                }
                return true;
            }
        });

        holder.itemView.setTag(item);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView note_title;
        public RelativeLayout parent;

        public ViewHolder(View itemView) {
            super(itemView);
            note_title = (TextView) itemView.findViewById(R.id.note_title);
            parent = (RelativeLayout) itemView.findViewById(R.id.parent);
        }
    }
}
