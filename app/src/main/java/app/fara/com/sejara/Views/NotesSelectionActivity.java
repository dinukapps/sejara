package app.fara.com.sejara.Views;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import app.fara.com.sejara.Adapters.NotesRecyclerAdapter;
import app.fara.com.sejara.Entity.Note;
import app.fara.com.sejara.R;

public class NotesSelectionActivity extends AppCompatActivity {

    private RecyclerView noteList;
    private String civil_id;
    private DatabaseReference mDatabase;
    private RelativeLayout backBtn, rlProgress;
    private ArrayList<Note> notes = new ArrayList<>();
    private NotesRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_selection);

        civil_id = getIntent().getStringExtra("civil_id");

        NotesSelectionActivity.this.getSupportActionBar().hide();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        noteList = (RecyclerView) findViewById(R.id.noteList);
        backBtn = (RelativeLayout) findViewById(R.id.backBtn);

        rlProgress.setVisibility(View.VISIBLE);

        new LoadNotes().execute();

        backBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        backBtn.setBackgroundColor(Color.parseColor("#009AFC"));
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        backBtn.setBackgroundColor(Color.parseColor("#009cff"));
                        break;
                    case MotionEvent.ACTION_UP:
                        backBtn.setBackgroundColor(Color.parseColor("#009cff"));

                        finish();

                        break;
                }
                return true;
            }
        });

    }

    class LoadNotes extends AsyncTask<Void, Void, Void> {

        LoadNotes() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            notes.clear();

            mDatabase.child("notes").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                        Note note = postSnapshot.getValue(Note.class);
                        notes.add(note);

                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new NotesRecyclerAdapter(notes, NotesSelectionActivity.this);
                            noteList.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NotesSelectionActivity.this);
                            noteList.setLayoutManager(mLayoutManager);
                            noteList.addItemDecoration(new DividerItemDecoration(NotesSelectionActivity.this, LinearLayoutManager.VERTICAL));
                            noteList.setItemAnimator(new DefaultItemAnimator());
                            noteList.setAdapter(adapter);
                            rlProgress.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }

}
