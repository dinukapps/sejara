package app.fara.com.sejara.Views;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.fara.com.sejara.Entity.Chapter;
import app.fara.com.sejara.R;
import app.fara.com.sejara.Views.CardViewer.CardFragmentPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.CardItem;
import app.fara.com.sejara.Views.CardViewer.CardPagerAdapter;
import app.fara.com.sejara.Views.CardViewer.ShadowTransformer;

public class MyNotesView extends AppCompatActivity {

    private ViewPager mViewPager;
    private RelativeLayout backBtn;
    private CardPagerAdapter mCardAdapter;
    private ShadowTransformer mCardShadowTransformer;
    private CardFragmentPagerAdapter mFragmentCardAdapter;
    private ShadowTransformer mFragmentCardShadowTransformer;
    private DatabaseReference mDatabase;
    private int chapterCount = 0;
    private RelativeLayout rlProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notes_view);

        MyNotesView.this.getSupportActionBar().hide();

        mDatabase = FirebaseDatabase.getInstance().getReference();

        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        backBtn = (RelativeLayout) findViewById(R.id.backBtn);
        mCardAdapter = new CardPagerAdapter(MyNotesView.this);

        rlProgress.setVisibility(View.VISIBLE);

        new LoadChapters().execute();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    class LoadChapters extends AsyncTask<Void, Void, Void> {

        LoadChapters() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            mDatabase.child("chapters").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Chapter chapter = postSnapshot.getValue(Chapter.class);
                        mCardAdapter.addCardItem(new CardItem(chapter.getID(), chapter.getName(), R.string.text_1));
                        chapterCount++;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mFragmentCardAdapter = new CardFragmentPagerAdapter(chapterCount, getSupportFragmentManager(), dpToPixels(2, MyNotesView.this));
                            mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);
                            mFragmentCardShadowTransformer = new ShadowTransformer(mViewPager, mFragmentCardAdapter);

                            mViewPager.setAdapter(mCardAdapter);
                            mViewPager.setPageTransformer(false, mCardShadowTransformer);
                            mViewPager.setOffscreenPageLimit(3);
                            mCardShadowTransformer.enableScaling(true);

                            rlProgress.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            return null;
        }

    }
}
