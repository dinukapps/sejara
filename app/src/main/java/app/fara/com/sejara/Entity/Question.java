package app.fara.com.sejara.Entity;

import java.util.ArrayList;

public class Question {
    private String Question = "";
    private int CorrectAnswer = 0;
    private ArrayList<String> Answers = new ArrayList<>();

    public Question() {

    }

    public Question(String question, int correctAnswer, ArrayList<String> answers) {
        Question = question;
        CorrectAnswer = correctAnswer;
        Answers = answers;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public ArrayList<String> getAnswers() {
        return Answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        Answers = answers;
    }
}
